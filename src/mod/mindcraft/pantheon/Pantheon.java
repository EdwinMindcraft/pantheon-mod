package mod.mindcraft.pantheon;

import mod.mindcraft.pantheon.common.ItemPickaxeMulti;
import mod.mindcraft.pantheon.cronos.blocks.BlockCronosDirt;
import mod.mindcraft.pantheon.cronos.blocks.BlockCronosGrass;
import mod.mindcraft.pantheon.cronos.blocks.BlockCronosLeaves;
import mod.mindcraft.pantheon.cronos.blocks.BlockCronosLog;
import mod.mindcraft.pantheon.cronos.blocks.BlockCronosSapling;
import mod.mindcraft.pantheon.cronos.creativetab.CreativeTabCronos;
import mod.mindcraft.pantheon.gaia.blocks.BlockGaiaDirt;
import mod.mindcraft.pantheon.gaia.blocks.BlockGaiaGrass;
import mod.mindcraft.pantheon.gaia.blocks.BlockGaiaLeaves;
import mod.mindcraft.pantheon.gaia.blocks.BlockGaiaLog;
import mod.mindcraft.pantheon.gaia.blocks.BlockGaiaSapling;
import mod.mindcraft.pantheon.gaia.creativetab.CreativeTabGaia;
import mod.mindcraft.pantheon.gaiavirus.blocks.BlockGaiaVirusDirt;
import mod.mindcraft.pantheon.gaiavirus.blocks.BlockGaiaVirusGrass;
import mod.mindcraft.pantheon.gaiavirus.blocks.BlockGaiaVirusLeaves;
import mod.mindcraft.pantheon.gaiavirus.blocks.BlockGaiaVirusLog;
import mod.mindcraft.pantheon.gaiavirus.blocks.BlockGaiaVirusSapling;
import mod.mindcraft.pantheon.gaiavirus.creativetab.CreativeTabGaiaVirus;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemPiston;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraftforge.common.util.EnumHelper;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;

@Mod(modid="mpantheon", name="Mindcraft's Pantheon Mod", version="WIP -- 0.0.1")
public class Pantheon {
	
	//Tool Materials
	
	public static final ToolMaterial GAIA_WOOD = EnumHelper.addToolMaterial("GaiaWood", 0, 128, 1.5F, 2, 10);
	public static final ToolMaterial GAIA_VIRUS_WOOD = EnumHelper.addToolMaterial("GaiaVirusWood", 0, 128, 3.0F, 2, 10);
	public static final ToolMaterial CRONOS_WOOD = EnumHelper.addToolMaterial("CronosWood", 0, 32, 6.0F, 2, 10);
	
	//Creative Tabs
	
	public static CreativeTabs tabGaia = new CreativeTabGaia();
	public static CreativeTabs tabGaiaVirus = new CreativeTabGaiaVirus();
	public static CreativeTabs tabCronos = new CreativeTabCronos();
	
	//Blocks
	
	public static Block blockGaiaDirt = new BlockGaiaDirt().setBlockName("gaia_dirt").setHardness(0.5F).setStepSound(Block.soundTypeGravel).setCreativeTab(tabGaia);
	public static Block blockGaiaGrass = new BlockGaiaGrass().setBlockName("gaia_grass").setHardness(0.6F).setStepSound(Block.soundTypeGrass).setCreativeTab(tabGaia);
	public static Block blockGaiaLeaves = new BlockGaiaLeaves().setBlockName("gaia_leaves").setCreativeTab(tabGaia);
	public static Block blockGaiaLog = new BlockGaiaLog().setBlockName("gaia_log").setCreativeTab(tabGaia);
	public static Block blockGaiaSapling = new BlockGaiaSapling().setBlockName("gaia_sapling").setCreativeTab(tabGaia).setStepSound(Block.soundTypeGrass);

	public static Block blockGaiaVirusDirt = new BlockGaiaVirusDirt().setBlockName("gaia_virus_dirt").setHardness(0.5F).setStepSound(Block.soundTypeGravel).setCreativeTab(tabGaiaVirus);
	public static Block blockGaiaVirusGrass = new BlockGaiaVirusGrass().setBlockName("gaia_virus_grass").setHardness(0.6F).setStepSound(Block.soundTypeGrass).setCreativeTab(tabGaiaVirus);
	public static Block blockGaiaVirusLeaves = new BlockGaiaVirusLeaves().setBlockName("gaia_virus_leaves").setCreativeTab(tabGaiaVirus);
	public static Block blockGaiaVirusLog = new BlockGaiaVirusLog().setBlockName("gaia_virus_log").setCreativeTab(tabGaiaVirus);
	public static Block blockGaiaVirusSapling = new BlockGaiaVirusSapling().setBlockName("gaia_virus_sapling").setStepSound(Block.soundTypeGrass).setCreativeTab(tabGaiaVirus);

	public static Block blockCronosDirt = new BlockCronosDirt().setBlockName("cronos_dirt").setHardness(0.5F).setStepSound(Block.soundTypeGravel).setCreativeTab(tabCronos);
	public static Block blockCronosGrass = new BlockCronosGrass().setBlockName("cronos_grass").setHardness(0.6F).setStepSound(Block.soundTypeGrass).setCreativeTab(tabCronos);
	public static Block blockCronosLeaves = new BlockCronosLeaves().setBlockName("cronos_leaves").setCreativeTab(tabCronos);
	public static Block blockCronosLog = new BlockCronosLog().setBlockName("cronos_log").setCreativeTab(tabCronos);
	public static Block blockCronosSapling = new BlockCronosSapling().setBlockName("cronos_sapling").setStepSound(Block.soundTypeGrass).setCreativeTab(tabCronos);
	
	//Items
	
	public static Item itemGaiaStick = new Item().setTextureName("mpantheon:gaia/gaia_stick").setCreativeTab(tabGaia).setUnlocalizedName("gaia_stick");

	public static Item itemGaiaVirusStick = new Item().setTextureName("mpantheon:gaiavirus/gaia_virus_stick").setCreativeTab(tabGaiaVirus).setUnlocalizedName("gaia_virus_stick");

	public static Item itemCronosStick = new Item().setTextureName("mpantheon:cronos/cronos_stick").setCreativeTab(tabCronos).setUnlocalizedName("cronos_stick");
	
	//Tools
	
	public static Item pickaxeGaiaWood = new ItemPickaxeMulti(GAIA_WOOD).setTextureName("mpantheon:gaia/gaia_wood_pickaxe").setCreativeTab(tabGaia).setUnlocalizedName("gaia_wood_pickaxe");

	public static Item pickaxeGaiaVirusWood = new ItemPickaxeMulti(GAIA_VIRUS_WOOD).setTextureName("mpantheon:gaiavirus/gaia_virus_wood_pickaxe").setCreativeTab(tabGaiaVirus).setUnlocalizedName("gaia_virus_wood_pickaxe");

	public static Item pickaxeCronosWood = new ItemPickaxeMulti(CRONOS_WOOD).setTextureName("mpantheon:cronos/cronos_wood_pickaxe").setCreativeTab(tabCronos).setUnlocalizedName("cronos_wood_pickaxe");
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent e) {
	}

	@EventHandler
	public void init(FMLInitializationEvent e) {
		defineGaia();
		defineGaiaVirus();
		defineCronos();
		
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent e) {
		
	}
	
	public void defineGaia() {
		GameRegistry.registerBlock(blockGaiaDirt, "gaia_dirt");
		GameRegistry.registerBlock(blockGaiaGrass, "gaia_grass");
		GameRegistry.registerBlock(blockGaiaLog, "gaia_log");
		GameRegistry.registerBlock(blockGaiaLeaves, "gaia_leaves");
		GameRegistry.registerBlock(blockGaiaSapling, "gaia_sapling");
				
		GameRegistry.registerItem(itemGaiaStick, "gaia_stick");
		
		blockGaiaDirt.setHarvestLevel("shovel", -1);
		blockGaiaGrass.setHarvestLevel("shovel", -1);
		blockGaiaLog.setHarvestLevel("axe", -1);
		
		GameRegistry.registerItem(pickaxeGaiaWood, "gaia_wood_pickaxe");
	}
	
	public void defineGaiaVirus() {
		GameRegistry.registerBlock(blockGaiaVirusDirt, "gaia_virus_dirt");
		GameRegistry.registerBlock(blockGaiaVirusGrass, "gaia_virus_grass");
		GameRegistry.registerBlock(blockGaiaVirusLog, "gaia_virus_log");
		GameRegistry.registerBlock(blockGaiaVirusLeaves, "gaia_virus_leaves");
		GameRegistry.registerBlock(blockGaiaVirusSapling, "gaia_virus_sapling");
		
		GameRegistry.registerItem(itemGaiaVirusStick, "gaia_virus_stick");
		
		blockGaiaVirusDirt.setHarvestLevel("shovel", -1);
		blockGaiaVirusGrass.setHarvestLevel("shovel", -1);
		blockGaiaVirusLog.setHarvestLevel("axe", -1);
		
		GameRegistry.registerItem(pickaxeGaiaVirusWood, "gaia_virus_wood_pickaxe");
	}
	
	public void defineCronos() {
		GameRegistry.registerBlock(blockCronosDirt, "cronos_dirt");
		GameRegistry.registerBlock(blockCronosGrass, "cronos_grass");
		GameRegistry.registerBlock(blockCronosLog, "cronos_log");
		GameRegistry.registerBlock(blockCronosLeaves, "cronos_leaves");
		GameRegistry.registerBlock(blockCronosSapling, "cronos_sapling");
		
		GameRegistry.registerItem(itemCronosStick, "cronos_stick");

		blockCronosDirt.setHarvestLevel("shovel", -1);
		blockCronosGrass.setHarvestLevel("shovel", -1);
		blockCronosLog.setHarvestLevel("axe", -1);

		GameRegistry.registerItem(pickaxeCronosWood, "cronos_wood_pickaxe");
	}
}
