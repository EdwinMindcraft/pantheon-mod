package mod.mindcraft.pantheon.cronos.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.util.IIcon;

public class BlockCronosDirt extends Block{

	IIcon cronosDirtTexture;
	
	public BlockCronosDirt() {
		super(Material.ground);
	}
	
	@Override
	public IIcon getIcon(int side, int metadata) {
		return cronosDirtTexture;
	}
	
	@Override
	public void registerBlockIcons(IIconRegister register) {
		cronosDirtTexture = register.registerIcon("mpantheon:cronos/cronos_dirt");
	}
}
