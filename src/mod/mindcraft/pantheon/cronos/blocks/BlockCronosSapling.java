package mod.mindcraft.pantheon.cronos.blocks;

import java.util.Random;

import mod.mindcraft.pantheon.Pantheon;
import mod.mindcraft.pantheon.cronos.worldgen.WorldGenCronosTree;
import net.minecraft.block.Block;
import net.minecraft.block.BlockBush;
import net.minecraft.block.IGrowable;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public class BlockCronosSapling extends BlockBush  implements IGrowable{
	
	IIcon cronosSaplingTexture;
	
    protected boolean canPlaceBlockOn(Block block)
    {
        return block == Pantheon.blockCronosGrass || block == Pantheon.blockCronosDirt;
    }
    
    public void updateTick(World world, int x, int y, int z, Random rand)
    {
        if (!world.isRemote)
        {
            super.updateTick(world, x, y, z, rand);
            generate(world, x, y, z, rand);
        }
    }

	@Override
	public boolean func_149851_a(World p_149851_1_, int p_149851_2_, int p_149851_3_, int p_149851_4_, boolean p_149851_5_) {

		return true;
	}

	@Override
	public boolean func_149852_a(World p_149852_1_, Random p_149852_2_, int p_149852_3_, int p_149852_4_, int p_149852_5_) {
		return (double)p_149852_1_.rand.nextFloat() < 0.45D;
	}

	@Override
	public void func_149853_b(World world, Random rand, int x, int y, int z) {
		this.BoneMealHandle(world, x, y, z, rand);
	}

	public void BoneMealHandle(World world, int x, int y, int z, Random rand) {
        this.generate(world, x, y, z, rand);		
	}
	
	public void generate (World world, int x, int y, int z, Random rand) {
        if (world.getBlockLightValue(x, y + 1, z) >= 9 && rand.nextInt(7) == 0)
        {
        	WorldGenCronosTree tree = new WorldGenCronosTree(true);
        	
            if (!tree.generate(world, rand, x, y, z))
            {
            	world.setBlock(x, y, z, this, 0, 4);
            }

        }
	}
	
	@Override
	public void registerBlockIcons(IIconRegister register) {
		cronosSaplingTexture = register.registerIcon("mpantheon:cronos/cronos_sapling");
	}
	
	@Override
	public IIcon getIcon(int p_149691_1_, int p_149691_2_) {
		return cronosSaplingTexture;
	}
}
