package mod.mindcraft.pantheon.cronos.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockLog;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.util.IIcon;

public class BlockCronosLog extends BlockLog{

	public BlockCronosLog() {
		super();
	}
		
	@Override
	public void registerBlockIcons(IIconRegister register) {
		this.field_150167_a = new IIcon[1];
		this.field_150166_b = new IIcon[1];
        this.field_150167_a[0] = register.registerIcon("mpantheon:cronos/cronos_log");
        this.field_150166_b[0] = register.registerIcon("mpantheon:cronos/cronos_log_top");

	}
}
