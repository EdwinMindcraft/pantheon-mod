package mod.mindcraft.pantheon.cronos.blocks;

import java.util.Random;

import mod.mindcraft.pantheon.Pantheon;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public class BlockCronosGrass extends Block {

	IIcon cronosGrassTexture_top;
	IIcon cronosGrassTexture_side;
	
	public BlockCronosGrass() {
		super(Material.grass);
		setTickRandomly(true);
	}

	@Override
	public void updateTick(World world, int x, int y, int z, Random rand) {
		if(!world.isRemote) {
			if ((world.getBlockLightValue(x, y+1, z) < 4) && (world.getBlockLightOpacity(x, y+1, z) > 2)) {
				world.setBlock(x, y, z, Pantheon.blockCronosDirt);
			}
			else if (world.getBlockLightValue(x, y + 1, z) >= 9)
            {
                for (int l = 0; l < 4; ++l)
                {
                    int i1 = x + rand.nextInt(3) - 1;
                    int j1 = y + rand.nextInt(5) - 3;
                    int k1 = z + rand.nextInt(3) - 1;
                    Block block = world.getBlock(i1, j1 + 1, k1);

                    if (world.getBlock(i1, j1, k1) == Pantheon.blockCronosDirt && world.getBlockMetadata(i1, j1, k1) == 0 && world.getBlockLightValue(i1, j1 + 1, k1) >= 4 && world.getBlockLightOpacity(i1, j1 + 1, k1) <= 2)
                    {
                        world.setBlock(i1, j1, k1, Pantheon.blockCronosGrass);
                    }
                }
            }
		}
	}
	
    public Item getItemDropped(int p_149650_1_, Random p_149650_2_, int p_149650_3_)
    {
        return Pantheon.blockCronosDirt.getItemDropped(0, p_149650_2_, p_149650_3_);
    }

	@Override
	public IIcon getIcon(int side, int metadata) {
		switch (side) {
		case 0: return Pantheon.blockCronosDirt.getBlockTextureFromSide(0);
		case 1: return cronosGrassTexture_top;
		default: return cronosGrassTexture_side;
		}
	}
	
	@Override
	public void registerBlockIcons(IIconRegister register) {
		cronosGrassTexture_side = register.registerIcon("mpantheon:cronos/cronos_grass_side");
		cronosGrassTexture_top = register.registerIcon("mpantheon:cronos/cronos_grass_top");
	}

}
