package mod.mindcraft.pantheon.cronos.creativetab;

import mod.mindcraft.pantheon.Pantheon;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class CreativeTabCronos extends CreativeTabs {

	public CreativeTabCronos() {
		super("cronos");
	}

	@Override
	public Item getTabIconItem() {
		return Item.getItemFromBlock(Pantheon.blockCronosGrass);
	}

}
