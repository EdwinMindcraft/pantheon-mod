package mod.mindcraft.pantheon.gaiavirus.blocks;

import java.util.Random;

import mod.mindcraft.pantheon.Pantheon;
import net.minecraft.block.BlockLeaves;
import net.minecraft.block.BlockOldLeaf;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;
import net.minecraft.util.IIcon;

public class BlockGaiaVirusLeaves extends BlockLeaves{
	
	
	IIcon[] GaiaVirusLeavesTexture;
	
	String field_150131_O[] = {"gaiavirus"};
	
	@Override
	public IIcon getIcon(int p_149691_1_, int p_149691_2_) {

		return field_150129_M[field_150127_b][0];
	}

    public String[] func_150125_e()
    {
        return field_150131_O;
    }
    
    @Override
    public void registerBlockIcons(IIconRegister register) {
    	this.field_150129_M[0] = new IIcon[1];
    	this.field_150129_M[1] = new IIcon[1];
    	field_150129_M[0][0] = register.registerIcon("mpantheon:gaiavirus/gaia_virus_leaves");
    	field_150129_M[1][0] = register.registerIcon("mpantheon:gaiavirus/gaia_virus_leaves_opaque");
    }
    
    @Override
    public boolean isOpaqueCube() {
    	return false;
    }
    
    @Override
    public Item getItemDropped(int p_149650_1_, Random p_149650_2_, int p_149650_3_) {
    	
    	return Item.getItemFromBlock(Pantheon.blockGaiaVirusSapling);
    }
}
