package mod.mindcraft.pantheon.gaiavirus.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.util.IIcon;

public class BlockGaiaVirusDirt extends Block{

	IIcon gaiaDirtTexture;
	
	public BlockGaiaVirusDirt() {
		super(Material.ground);
	}
	
	@Override
	public IIcon getIcon(int side, int metadata) {
		return gaiaDirtTexture;
	}
	
	@Override
	public void registerBlockIcons(IIconRegister register) {
		gaiaDirtTexture = register.registerIcon("mpantheon:gaiavirus/gaia_virus_dirt");
	}
}
