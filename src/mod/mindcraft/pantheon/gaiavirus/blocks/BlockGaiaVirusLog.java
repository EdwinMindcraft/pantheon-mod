package mod.mindcraft.pantheon.gaiavirus.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockLog;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.util.IIcon;

public class BlockGaiaVirusLog extends BlockLog{
	
	public BlockGaiaVirusLog() {
		super();
	}
		
	@Override
	public void registerBlockIcons(IIconRegister register) {
		this.field_150167_a = new IIcon[1];
		this.field_150166_b = new IIcon[1];
        this.field_150167_a[0] = register.registerIcon("mpantheon:gaiavirus/gaia_virus_log");
        this.field_150166_b[0] = register.registerIcon("mpantheon:gaiavirus/gaia_virus_log_top");

	}
}
