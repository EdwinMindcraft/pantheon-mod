package mod.mindcraft.pantheon.gaiavirus.blocks;

import java.util.Random;

import mod.mindcraft.pantheon.Pantheon;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public class BlockGaiaVirusGrass extends Block {

	IIcon gaiaVirusGrassTexture_top;
	IIcon gaiaVirusGrassTexture_side;
	
	public BlockGaiaVirusGrass() {
		super(Material.grass);
		setTickRandomly(true);
	}

	@Override
	public void updateTick(World world, int x, int y, int z, Random rand) {
		if(!world.isRemote) {
			if ((world.getBlockLightValue(x, y+1, z) < 4) && (world.getBlockLightOpacity(x, y+1, z) > 2)) {
				world.setBlock(x, y, z, Pantheon.blockGaiaVirusDirt);
			}
			else if (world.getBlockLightValue(x, y + 1, z) >= 9)
            {
                for (int l = 0; l < 4; ++l)
                {
                    int i1 = x + rand.nextInt(3) - 1;
                    int j1 = y + rand.nextInt(5) - 3;
                    int k1 = z + rand.nextInt(3) - 1;
                    Block block = world.getBlock(i1, j1 + 1, k1);

                    if (world.getBlock(i1, j1, k1) == Pantheon.blockGaiaVirusDirt && world.getBlockMetadata(i1, j1, k1) == 0 && world.getBlockLightValue(i1, j1 + 1, k1) >= 4 && world.getBlockLightOpacity(i1, j1 + 1, k1) <= 2)
                    {
                        world.setBlock(i1, j1, k1, Pantheon.blockGaiaVirusGrass);
                    }
                }
            }
		}
	}
	
    public Item getItemDropped(int p_149650_1_, Random p_149650_2_, int p_149650_3_)
    {
        return Pantheon.blockGaiaVirusDirt.getItemDropped(0, p_149650_2_, p_149650_3_);
    }

	@Override
	public IIcon getIcon(int side, int metadata) {
		switch (side) {
		case 0: return Pantheon.blockGaiaVirusDirt.getBlockTextureFromSide(0);
		case 1: return gaiaVirusGrassTexture_top;
		default: return gaiaVirusGrassTexture_side;
		}
	}
	
	@Override
	public void registerBlockIcons(IIconRegister register) {
		gaiaVirusGrassTexture_side = register.registerIcon("mpantheon:gaiavirus/gaia_virus_grass_side");
		gaiaVirusGrassTexture_top = register.registerIcon("mpantheon:gaiavirus/gaia_virus_grass_top");
	}

}
