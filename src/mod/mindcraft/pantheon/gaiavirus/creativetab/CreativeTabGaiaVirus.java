package mod.mindcraft.pantheon.gaiavirus.creativetab;

import mod.mindcraft.pantheon.Pantheon;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class CreativeTabGaiaVirus extends CreativeTabs {

	public CreativeTabGaiaVirus() {
		super("gaiavirus");
	}

	@Override
	public Item getTabIconItem() {
		return Item.getItemFromBlock(Pantheon.blockGaiaVirusGrass);
	}

}
