package mod.mindcraft.pantheon.gaia.worldgen;

import java.util.Random;

import mod.mindcraft.pantheon.Pantheon;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenAbstractTree;

public class WorldGenGaiaTree extends WorldGenAbstractTree {

	int minHeight = 4;
	Block leaves = Pantheon.blockGaiaLeaves;
	Block log = Pantheon.blockGaiaLog;
	
	public WorldGenGaiaTree(boolean bool) {
		super(bool);
	}

	@Override
	public boolean generate(World world, Random rand, int i, int y, int k) {
		
		int v = rand.nextInt(3) + 1;
		int j = y + v;
		
		for(int g = 1; g <= v; g++) {
			if (world.getBlock(i, y + g, k).getMaterial() == Material.leaves || world.getBlock(i, y + g, k) == Blocks.air) {
			}
			else return false;
		}
		if (!isGenPossible(world, i, j, k)) return false;
		
		for(int g = 0; g <= v; g++) {
			world.setBlock(i, y + g, k, log);
		}

		world.setBlock(i - 2, j + 0, k - 2, leaves);
		world.setBlock(i - 2, j + 0, k - 1, leaves);
		world.setBlock(i - 2, j + 0, k, leaves);
		world.setBlock(i - 2, j + 0, k + 1, leaves);
		world.setBlock(i - 2, j + 0, k + 2, leaves);
		world.setBlock(i - 2, j + 1, k - 1, leaves);
		world.setBlock(i - 2, j + 1, k, leaves);
		world.setBlock(i - 2, j + 1, k + 1, leaves);
		world.setBlock(i - 1, j + 0, k - 2, leaves);
		world.setBlock(i - 1, j + 0, k - 1, leaves);
		world.setBlock(i - 1, j + 0, k, leaves);
		world.setBlock(i - 1, j + 0, k + 1, leaves);
		world.setBlock(i - 1, j + 0, k + 2, leaves);
		world.setBlock(i - 1, j + 1, k - 2, leaves);
		world.setBlock(i - 1, j + 1, k - 1, leaves);
		world.setBlock(i - 1, j + 1, k, leaves);
		world.setBlock(i - 1, j + 1, k + 1, leaves);
		world.setBlock(i - 1, j + 1, k + 2, leaves);
		world.setBlock(i - 1, j + 2, k, leaves);
		world.setBlock(i - 1, j + 3, k, leaves);
		world.setBlock(i , j + 0, k - 2, leaves);
		world.setBlock(i , j + 0, k - 1, leaves);
		world.setBlock(i , j + 0, k, log);
		world.setBlock(i , j + 0, k + 1, leaves);
		world.setBlock(i , j + 0, k + 2, leaves);
		world.setBlock(i , j + 1, k - 2, leaves);
		world.setBlock(i , j + 1, k - 1, leaves);
		world.setBlock(i , j + 1, k, log);
		world.setBlock(i , j + 1, k + 1, leaves);
		world.setBlock(i , j + 1, k + 2, leaves);
		world.setBlock(i , j + 2, k - 1, leaves);
		world.setBlock(i , j + 2, k, log);
		world.setBlock(i , j + 2, k + 1, leaves);
		world.setBlock(i , j + 3, k - 1, leaves);
		world.setBlock(i , j + 3, k, leaves);
		world.setBlock(i , j + 3, k + 1, leaves);
		world.setBlock(i + 1, j + 0, k - 2, leaves);
		world.setBlock(i + 1, j + 0, k - 1, leaves);
		world.setBlock(i + 1, j + 0, k, leaves);
		world.setBlock(i + 1, j + 0, k + 1, leaves);
		world.setBlock(i + 1, j + 0, k + 2, leaves);
		world.setBlock(i + 1, j + 1, k - 2, leaves);
		world.setBlock(i + 1, j + 1, k - 1, leaves);
		world.setBlock(i + 1, j + 1, k, leaves);
		world.setBlock(i + 1, j + 1, k + 1, leaves);
		world.setBlock(i + 1, j + 1, k + 2, leaves);
		world.setBlock(i + 1, j + 2, k, leaves);
		world.setBlock(i + 1, j + 2, k + 1, leaves);
		world.setBlock(i + 1, j + 3, k, leaves);
		world.setBlock(i + 2, j + 0, k - 2, leaves);
		world.setBlock(i + 2, j + 0, k - 1, leaves);
		world.setBlock(i + 2, j + 0, k, leaves);
		world.setBlock(i + 2, j + 0, k + 1, leaves);
		world.setBlock(i + 2, j + 0, k + 2, leaves);
		world.setBlock(i + 2, j + 1, k - 1, leaves);
		world.setBlock(i + 2, j + 1, k, leaves);
		world.setBlock(i + 2, j + 1, k + 1, leaves);
		return true;
	}
	
	public boolean isGenPossible (World world, int i, int j, int k) {
		
		boolean returnedState = true;
		boolean [] checkList = {
		world.getBlock(i + 0, j + 0, k + 0) == Blocks.air,
		world.getBlock(i + 0, j + 0, k + 1) == Blocks.air,
		world.getBlock(i + 0, j + 0, k + 2) == Blocks.air,
		world.getBlock(i + 0, j + 0, k + 3) == Blocks.air,
		world.getBlock(i + 0, j + 0, k + 4) == Blocks.air,
		world.getBlock(i + 0, j + 1, k + 1) == Blocks.air,
		world.getBlock(i + 0, j + 1, k + 2) == Blocks.air,
		world.getBlock(i + 0, j + 1, k + 3) == Blocks.air,
		world.getBlock(i + 1, j + 0, k + 0) == Blocks.air,
		world.getBlock(i + 1, j + 0, k + 1) == Blocks.air,
		world.getBlock(i + 1, j + 0, k + 2) == Blocks.air,
		world.getBlock(i + 1, j + 0, k + 3) == Blocks.air,
		world.getBlock(i + 1, j + 0, k + 4) == Blocks.air,
		world.getBlock(i + 1, j + 1, k + 0) == Blocks.air,
		world.getBlock(i + 1, j + 1, k + 1) == Blocks.air,
		world.getBlock(i + 1, j + 1, k + 2) == Blocks.air,
		world.getBlock(i + 1, j + 1, k + 3) == Blocks.air,
		world.getBlock(i + 1, j + 1, k + 4) == Blocks.air,
		world.getBlock(i + 1, j + 2, k + 2) == Blocks.air,
		world.getBlock(i + 1, j + 3, k + 2) == Blocks.air,
		world.getBlock(i + 2, j + 0, k + 0) == Blocks.air,
		world.getBlock(i + 2, j + 0, k + 1) == Blocks.air,
		world.getBlock(i + 2, j + 0, k + 2) == Blocks.air,
		world.getBlock(i + 2, j + 0, k + 3) == Blocks.air,
		world.getBlock(i + 2, j + 0, k + 4) == Blocks.air,
		world.getBlock(i + 2, j + 1, k + 0) == Blocks.air,
		world.getBlock(i + 2, j + 1, k + 1) == Blocks.air,
		world.getBlock(i + 2, j + 1, k + 2) == Blocks.air,
		world.getBlock(i + 2, j + 1, k + 3) == Blocks.air,
		world.getBlock(i + 2, j + 1, k + 4) == Blocks.air,
		world.getBlock(i + 2, j + 2, k + 1) == Blocks.air,
		world.getBlock(i + 2, j + 2, k + 2) == Blocks.air,
		world.getBlock(i + 2, j + 2, k + 3) == Blocks.air,
		world.getBlock(i + 2, j + 3, k + 1) == Blocks.air,
		world.getBlock(i + 2, j + 3, k + 2) == Blocks.air,
		world.getBlock(i + 2, j + 3, k + 3) == Blocks.air,
		world.getBlock(i + 3, j + 0, k + 0) == Blocks.air,
		world.getBlock(i + 3, j + 0, k + 1) == Blocks.air,
		world.getBlock(i + 3, j + 0, k + 2) == Blocks.air,
		world.getBlock(i + 3, j + 0, k + 3) == Blocks.air,
		world.getBlock(i + 3, j + 0, k + 4) == Blocks.air,
		world.getBlock(i + 3, j + 1, k + 0) == Blocks.air,
		world.getBlock(i + 3, j + 1, k + 1) == Blocks.air,
		world.getBlock(i + 3, j + 1, k + 2) == Blocks.air,
		world.getBlock(i + 3, j + 1, k + 3) == Blocks.air,
		world.getBlock(i + 3, j + 1, k + 4) == Blocks.air,
		world.getBlock(i + 3, j + 2, k + 2) == Blocks.air,
		world.getBlock(i + 3, j + 2, k + 3) == Blocks.air,
		world.getBlock(i + 3, j + 3, k + 2) == Blocks.air,
		world.getBlock(i + 4, j + 0, k + 0) == Blocks.air,
		world.getBlock(i + 4, j + 0, k + 1) == Blocks.air,
		world.getBlock(i + 4, j + 0, k + 2) == Blocks.air,
		world.getBlock(i + 4, j + 0, k + 3) == Blocks.air,
		world.getBlock(i + 4, j + 0, k + 4) == Blocks.air,
		world.getBlock(i + 4, j + 1, k + 1) == Blocks.air,
		world.getBlock(i + 4, j + 1, k + 2) == Blocks.air,
		world.getBlock(i + 4, j + 1, k + 3) == Blocks.air};
		boolean checklist2 [] = {
		world.getBlock(i + 0, j + 0, k + 0).getMaterial() == Material.leaves,
		world.getBlock(i + 0, j + 0, k + 1).getMaterial() == Material.leaves,
		world.getBlock(i + 0, j + 0, k + 2).getMaterial() == Material.leaves,
		world.getBlock(i + 0, j + 0, k + 3).getMaterial() == Material.leaves,
		world.getBlock(i + 0, j + 0, k + 4).getMaterial() == Material.leaves,
		world.getBlock(i + 0, j + 1, k + 1).getMaterial() == Material.leaves,
		world.getBlock(i + 0, j + 1, k + 2).getMaterial() == Material.leaves,
		world.getBlock(i + 0, j + 1, k + 3).getMaterial() == Material.leaves,
		world.getBlock(i + 1, j + 0, k + 0).getMaterial() == Material.leaves,
		world.getBlock(i + 1, j + 0, k + 1).getMaterial() == Material.leaves,
		world.getBlock(i + 1, j + 0, k + 2).getMaterial() == Material.leaves,
		world.getBlock(i + 1, j + 0, k + 3).getMaterial() == Material.leaves,
		world.getBlock(i + 1, j + 0, k + 4).getMaterial() == Material.leaves,
		world.getBlock(i + 1, j + 1, k + 0).getMaterial() == Material.leaves,
		world.getBlock(i + 1, j + 1, k + 1).getMaterial() == Material.leaves,
		world.getBlock(i + 1, j + 1, k + 2).getMaterial() == Material.leaves,
		world.getBlock(i + 1, j + 1, k + 3).getMaterial() == Material.leaves,
		world.getBlock(i + 1, j + 1, k + 4).getMaterial() == Material.leaves,
		world.getBlock(i + 1, j + 2, k + 2).getMaterial() == Material.leaves,
		world.getBlock(i + 1, j + 3, k + 2).getMaterial() == Material.leaves,
		world.getBlock(i + 2, j + 0, k + 0).getMaterial() == Material.leaves,
		world.getBlock(i + 2, j + 0, k + 1).getMaterial() == Material.leaves,
		world.getBlock(i + 2, j + 0, k + 2).getMaterial() == Material.leaves,
		world.getBlock(i + 2, j + 0, k + 3).getMaterial() == Material.leaves,
		world.getBlock(i + 2, j + 0, k + 4).getMaterial() == Material.leaves,
		world.getBlock(i + 2, j + 1, k + 0).getMaterial() == Material.leaves,
		world.getBlock(i + 2, j + 1, k + 1).getMaterial() == Material.leaves,
		world.getBlock(i + 2, j + 1, k + 2).getMaterial() == Material.leaves,
		world.getBlock(i + 2, j + 1, k + 3).getMaterial() == Material.leaves,
		world.getBlock(i + 2, j + 1, k + 4).getMaterial() == Material.leaves,
		world.getBlock(i + 2, j + 2, k + 1).getMaterial() == Material.leaves,
		world.getBlock(i + 2, j + 2, k + 2).getMaterial() == Material.leaves,
		world.getBlock(i + 2, j + 2, k + 3).getMaterial() == Material.leaves,
		world.getBlock(i + 2, j + 3, k + 1).getMaterial() == Material.leaves,
		world.getBlock(i + 2, j + 3, k + 2).getMaterial() == Material.leaves,
		world.getBlock(i + 2, j + 3, k + 3).getMaterial() == Material.leaves,
		world.getBlock(i + 3, j + 0, k + 0).getMaterial() == Material.leaves,
		world.getBlock(i + 3, j + 0, k + 1).getMaterial() == Material.leaves,
		world.getBlock(i + 3, j + 0, k + 2).getMaterial() == Material.leaves,
		world.getBlock(i + 3, j + 0, k + 3).getMaterial() == Material.leaves,
		world.getBlock(i + 3, j + 0, k + 4).getMaterial() == Material.leaves,
		world.getBlock(i + 3, j + 1, k + 0).getMaterial() == Material.leaves,
		world.getBlock(i + 3, j + 1, k + 1).getMaterial() == Material.leaves,
		world.getBlock(i + 3, j + 1, k + 2).getMaterial() == Material.leaves,
		world.getBlock(i + 3, j + 1, k + 3).getMaterial() == Material.leaves,
		world.getBlock(i + 3, j + 1, k + 4).getMaterial() == Material.leaves,
		world.getBlock(i + 3, j + 2, k + 2).getMaterial() == Material.leaves,
		world.getBlock(i + 3, j + 2, k + 3).getMaterial() == Material.leaves,
		world.getBlock(i + 3, j + 3, k + 2).getMaterial() == Material.leaves,
		world.getBlock(i + 4, j + 0, k + 0).getMaterial() == Material.leaves,
		world.getBlock(i + 4, j + 0, k + 1).getMaterial() == Material.leaves,
		world.getBlock(i + 4, j + 0, k + 2).getMaterial() == Material.leaves,
		world.getBlock(i + 4, j + 0, k + 3).getMaterial() == Material.leaves,
		world.getBlock(i + 4, j + 0, k + 4).getMaterial() == Material.leaves,
		world.getBlock(i + 4, j + 1, k + 1).getMaterial() == Material.leaves,
		world.getBlock(i + 4, j + 1, k + 2).getMaterial() == Material.leaves,
		world.getBlock(i + 4, j + 1, k + 3).getMaterial() == Material.leaves };
		
		for (boolean current : checkList) {
			for (boolean current2 : checklist2) {
				if (current || current2) {
				}
				else returnedState = false;
			}
		}
		return returnedState;
		
	}

}
