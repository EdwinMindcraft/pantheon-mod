package mod.mindcraft.pantheon.gaia.blocks;

import java.util.Random;

import mod.mindcraft.pantheon.Pantheon;
import net.minecraft.block.BlockLeaves;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;
import net.minecraft.util.IIcon;

public class BlockGaiaLeaves extends BlockLeaves{
	
	
	IIcon GaiaLeavesTexture [];
	
	String field_150131_O[] = {"gaia"};
	
	@Override
	public IIcon getIcon(int p_149691_1_, int p_149691_2_) {
		return field_150129_M[field_150127_b][0];
	}

    public String[] func_150125_e()
    {
        return field_150131_O;
    }
    
    @Override
    public void registerBlockIcons(IIconRegister register) {
    	this.field_150129_M[0] = new IIcon[1];
    	this.field_150129_M[1] = new IIcon[1];
    	field_150129_M[0][0] = register.registerIcon("mpantheon:gaia/gaia_leaves");
    	field_150129_M[1][0] = register.registerIcon("mpantheon:gaia/gaia_leaves_opaque");
    }
    
    @Override
    public boolean isOpaqueCube() {
    	return false;
    }
    
    @Override
    public Item getItemDropped(int p_149650_1_, Random p_149650_2_, int p_149650_3_) {
    	
    	return Item.getItemFromBlock(Pantheon.blockGaiaSapling);
    }
}
