package mod.mindcraft.pantheon.gaia.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.util.IIcon;

public class BlockGaiaDirt extends Block{

	IIcon gaiaDirtTexture;
	
	public BlockGaiaDirt() {
		super(Material.ground);
	}
	
	@Override
	public IIcon getIcon(int side, int metadata) {
		return gaiaDirtTexture;
	}
	
	@Override
	public void registerBlockIcons(IIconRegister register) {
		gaiaDirtTexture = register.registerIcon("mpantheon:gaia/gaia_dirt");
	}
}
