package mod.mindcraft.pantheon.gaia.creativetab;

import mod.mindcraft.pantheon.Pantheon;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class CreativeTabGaia extends CreativeTabs {

	public CreativeTabGaia() {
		super("gaia");
	}

	@Override
	public Item getTabIconItem() {
		return Item.getItemFromBlock(Pantheon.blockGaiaGrass);
	}

}
